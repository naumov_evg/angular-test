import {AfterViewChecked, AfterViewInit, Component, ComponentRef, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Engine} from '@babylonjs/core';
import {SceneService} from './services/scene.service';
import {LightComponent} from './components/light/light.component';
import {SphereComponent} from './components/sphere/sphere.component';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
	@ViewChild('rootCanvas', { static: true })
	private canvas: ElementRef<HTMLCanvasElement>;

	@ViewChild('first')
	public firstSphere: SphereComponent;

	@ViewChild('second')
	public secondSphere: SphereComponent;

	public secondSphereValue = -1;

	constructor(private sceneService: SceneService) {
	}

	ngOnInit(): void {
		this.sceneService.init(this.canvas.nativeElement);
	}

	ngAfterViewInit(): void {
		setInterval(() => {
			this.secondSphere.x -= 0.01;
			this.firstSphere.x += 0.01;
		}, 50);
	}

}
