import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {GroundComponent} from './components/ground/ground.component';
import {SphereComponent} from './components/sphere/sphere.component';
import {LightComponent} from './components/light/light.component';
import {CameraComponent} from './components/camera/camera.component';

@NgModule({
	declarations: [
		AppComponent,
		GroundComponent,
		SphereComponent,
		LightComponent,
		CameraComponent,
	],
	imports: [
		BrowserModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
