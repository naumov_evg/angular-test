import {Component, Input, OnInit} from '@angular/core';
import {Camera, EventState, FreeCamera, Vector3} from '@babylonjs/core';
import {SceneService} from '../../services/scene.service';
import {CameraService} from '../../services/camera.service';

@Component({
	selector: 'app-camera',
	templateUrl: './camera.component.html',
	styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit {

	constructor(
		private sceneService: SceneService,
		private cameraService: CameraService
	) {
	}

	public camera: FreeCamera;

	ngOnInit(): void {
		// Create a FreeCamera, and set its position to (x:0, y:5, z:-10).
		const camera = new FreeCamera('camera', new Vector3(0, 5, -10), this.sceneService.scene);

		// Target the camera to scene origin.
		camera.setTarget(Vector3.Zero());

		// Attach the camera to the canvas.
		camera.attachControl(this.sceneService.canvas, false);

		camera.onViewMatrixChangedObservable.add((eventCamera: Camera, event: EventState) => {
			this.cameraService.changeCamera.next({
				camera: eventCamera,
				state: event
			});
		});

		this.camera = camera;
	}
}
