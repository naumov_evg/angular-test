import {Component, Input, OnInit} from '@angular/core';
import {Mesh, MeshBuilder} from '@babylonjs/core';
import {SceneService} from '../../services/scene.service';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import {CameraService} from '../../services/camera.service';

@Component({
	selector: 'app-sphere',
	templateUrl: './sphere.component.html',
	styleUrls: ['./sphere.component.css']
})
export class SphereComponent implements OnInit {

	constructor(
		private sceneService: SceneService,
		private cameraService: CameraService
	) {
	}

	private x$ = new ReplaySubject<number>(1);

	@Input() public set x(v: number) {
		this.x$.next(v);
	}

	public get x(): number {
		return this.sphere ? this.sphere.position.x : 0;
	}

	private sphere?: Mesh;

	ngOnInit(): void {
		console.log('sphere init');
		// Create a built-in "sphere" shape.
		this.sphere = MeshBuilder.CreateSphere('sphere', {segments: 16, diameter: 2}, this.sceneService.scene);

		this.x$.subscribe(x => {
			if (this.sphere) {
				this.sphere.position.x = x;
			}
		});
		this.sphere.position.y = 1;
		this.cameraService.changeCamera.subscribe(event => {
			console.log('sphere change camera event', event);
		});
	}

}
