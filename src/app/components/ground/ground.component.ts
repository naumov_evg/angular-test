import {Component, OnInit} from '@angular/core';
import {SceneService} from '../../services/scene.service';
import {MeshBuilder} from '@babylonjs/core';

@Component({
	selector: 'app-ground',
	templateUrl: './ground.component.html',
	styleUrls: ['./ground.component.css']
})
export class GroundComponent implements OnInit {

	constructor(private sceneService: SceneService) {
	}

	ngOnInit(): void {
		// Create a built-in "ground" shape.
		MeshBuilder.CreateGround('ground1', {height: 6, width: 6, subdivisions: 2}, this.sceneService.scene);
	}
}
