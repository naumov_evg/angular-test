import {Component, OnInit} from '@angular/core';
import {HemisphericLight, Vector3} from '@babylonjs/core';
import {SceneService} from '../../services/scene.service';

@Component({
	selector: 'app-light',
	templateUrl: './light.component.html',
	styleUrls: ['./light.component.css']
})
export class LightComponent implements OnInit {

	constructor(private sceneService: SceneService) {
	}

	ngOnInit(): void {
		const light = new HemisphericLight('light1', new Vector3(0, 1, 0), this.sceneService.scene);
	}

}
