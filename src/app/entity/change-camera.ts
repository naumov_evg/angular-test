import {Camera, EventState} from '@babylonjs/core';

export interface IChangeCamera {
	camera: Camera;
	state: EventState;
}
