import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {IChangeCamera} from '../entity/change-camera';

@Injectable({
	providedIn: 'root'
})
export class CameraService {
	public changeCamera = new Subject<IChangeCamera>();
}
