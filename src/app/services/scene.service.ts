import {Injectable} from '@angular/core';
import {Engine, Scene} from '@babylonjs/core';

@Injectable({
	providedIn: 'root'
})
export class SceneService {

	private _scene: Scene;
	private _engine: Engine;
	private _canvas: HTMLCanvasElement;

	constructor() {
	}

	init(canvas: HTMLCanvasElement) {
		this._canvas = canvas;
		this._engine = new Engine(canvas, true);
		this._scene = new Scene(this._engine);
		this._engine.runRenderLoop(() => {
			this._scene.render();
		});
		window.addEventListener('resize', () => {
			this.engine.resize();
		});
	}

	get scene(): Scene {
		return this._scene;
	}

	get engine(): Engine {
		return this._engine;
	}

	get canvas(): HTMLCanvasElement {
		return this._canvas;
	}
}
